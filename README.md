# vg-lvm-fs-mount

This simple role will handle several important tasks:
1. Disk partitioning
2. Volume groups creation
3. Logical volume creation
4. Filesystem creation
5. Filesystem mounts

Role will handle most of the changes online, such as: adding new disks,
logical volumes, increasing LV/FS size, even reducing disks from volume
group natively.

Of course, it will fail if task can't be done. For example, reducing VG for
more than space needed, or online reducing of FS (not supported by LVM).

Ansible offfers nice and stable modules to handle all LVM tasks out of the box.

This role is not ment to be "idiot proof". Instead, it is ment to provide ground
for a nice tool when properly used.

With that in mind it is divided in several modules (task files):

## LVG module

Responsible for partitioning of disks and creation of volume groups.
Expected role variabeles:
  - `vg_lvm_fs_parted_list`
  - `vg_lvm_fs_mount_vglist`

## LVM module

Responsible for logical volume creation on existing volume groups.
In order to do so, it will run only if `vg`, `lv` and `lv_size` elements defined
in role variable:
  - `vg_lvm_fs_mount_list`

## FS module

Responsible for filesystem creation.
Please note that, if filesystem type is XFS, we should change `flg_resizefs` to `no` due to
resising restrictions on XFS while mounted.

Module is checking if elements `vg`, `lv`, `lv_size` and `fs_type` are defined
in role variable:
  - `vg_lvm_fs_mount_list`

## MOUNT module

Responsible for mounting of filesystem. Mounted filesystems will be defined in `/etc/fstab`
file.

Module is checking if elements `vg`, `lv`, `fs`, `fs_type` and `mount_opt` are defined
in role variable:
  - `vg_lvm_fs_mount_list`


# Supported OS

The role is written for Debian Linux. If used on other Linux flavors, it needs to be checked
and updated, if needed.


# Role variables

## Defaults

    vg_lvm_fs_parted_list

Partition definition with device, partition number, type and part_end parameters.
Example:

```
vg_lvm_fs_parted_list:
  - device: "/dev/vdb"
    part_number: 1
    part_type: "primary"
    part_end: "100%"
    flags:
      - "lvm"
  - device: "/dev/vdc"
    part_number: 1
    part_type: "primary"
    part_end: "100%"
    flags:
      - "lvm"
```

    vg_lvm_fs_mount_vglist

Volume groups and its definitions. Contains volume group name, list of disks as members and
physical extent size.

Example:

```
vg_lvm_fs_mount_vglist:
  - vg: "vgbacula"
    disks:
      - /dev/vdb1
      - /dev/vdc1
    pesize: 32
```

    vg_lvm_fs_mount_list

Logical volume and filesystems list with definitions of logical volume name, size, filesystem
type, mount point and mount options.

Example:

```
vg_lvm_fs_mount_list:
  - lv: "lvpgsqldata"
    vg: "vgbacula"
    lv_size: "512M"
    fs: "/var/lib/postgresql"
    fs_type: "ext4"
    mount_opts: "defaults"
```

    flg_resizefs

Flag determinating if filesystem should be resized if defined size differs compared to filesystem on server.
Default is `yes`, but, sometimes we want to be able to control this parameter (xfs filesystem, for example).

## Variables

    vg_lvm_fs_pkgs

List of LVM related packages the role requires.
